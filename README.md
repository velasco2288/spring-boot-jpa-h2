# spring-boot-jpa-h2

# GET
http://localhost:8081/personas/findall

# POST  
http://localhost:8081/personas
```json
 {
 	"id":0,
 	"nombre":"devhoss",
 	"edad":32
 	
 }
```
# Pasos para compilar

mvn clean package 

# Pasos para compilar y ejecutar jar 

mvn clean package && java -jar target/spring-boot-jpa-h2-0.0.1-SNAPSHOT.jar

# Pasos para compilar y ejecutar jar y setear puerto 

mvn clean package && java -jar -Dserver.port=7098 target/spring-boot-jpa-h2-0.0.1-SNAPSHOT.jar


# Docker cuntruir una imagen
docker build -t spring-boot-jpa-h2 .

# DOcker ejecutar la imagen creada
docker run -p 5000:8080 spring-boot-jpa-h2
5000 puerto de la maquina
8080 puerto de container
 
 # docker ejecutar modo background

 docker run -d -p 8081:8081 spring-boot-jpa-h2

#  ver containers 
docker container ls
dokcer ps

# PRUEBAS

curl http://localhost:5000/personas/findall
